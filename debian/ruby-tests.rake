require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/test_warning.rb']
  t.test_files = FileList['test/test_freeze_warning.rb']
end
